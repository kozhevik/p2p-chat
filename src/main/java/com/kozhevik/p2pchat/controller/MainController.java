package com.kozhevik.p2pchat.controller;

import com.kozhevik.p2pchat.utils.InetSocketAddressUtils;
import com.kozhevik.p2pchat.utils.NodeErrLogger;
import com.kozhevik.p2pchat.browser.BrowserClient;
import com.kozhevik.p2pchat.browser.BrowserServer;
import com.kozhevik.p2pchat.p2p.Node;
import com.kozhevik.p2pchat.p2p.NodeListener;
import javafx.application.Platform;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

import java.net.InetSocketAddress;
import java.net.SocketException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Controller for working with chat
 */

public class MainController implements NodeListener {
    private final String SYSTEM_SENDER = "system";
    private final String COLOR_NORMAL = "black";
    private final String COLOR_BAD = "red";
    private final String COLOR_GOOD = "green";
    private final String COLOR_WARN = "orange";
    private final String SELF_SENDER_NAME = "me";

    private final Property<Node> nodeProperty = new SimpleObjectProperty<>();
    private final Timer peerDiscoveryTimer = new Timer();
    private BrowserServer browserServer;
    private Timer syncTimer;

    @FXML
    private ListView<String> peerListView;
    @FXML
    private TextField inputTextField;
    @FXML
    private ScrollPane chatScrollPane;
    @FXML
    private TextFlow chatTextFlow;

    @FXML
    private TextField selfPortTextField;
    @FXML
    private CheckBox connectToExistingChatCheckBox;
    @FXML
    private TextField peerAddressTextField;
    @FXML
    private TextField peerPortTextField;
    @FXML
    private Button connectivityButton;
    @FXML
    private TitledPane chatTitledPane;
    @FXML
    private ListView<String> discoveredPeersListView;

    public void init() {
        this.inputTextField.setOnKeyPressed(keyEvent -> {
            if (keyEvent.getCode().equals(KeyCode.ENTER)) {
                String message = inputTextField.getText();

//                if (message.isBlank()) {
//                    return;
//                }

                this.nodeProperty.getValue().send(message);
            }
        });

        this.connectToExistingChatCheckBox.selectedProperty().addListener((obs, old, connectToExisting) -> {
            connectivityButton.setText(connectToExisting ? "Connect" : "Listen");
            peerAddressTextField.setDisable(!connectToExisting);
            peerPortTextField.setDisable(!connectToExisting);
            discoveredPeersListView.setDisable(!connectToExisting);
        });

        this.connectivityButton.setOnAction(x -> {
            if (nodeProperty.getValue() == null) {
                String selfPortText = selfPortTextField.getText();

                Node node;
                try {
                    node = new Node(Integer.parseInt(selfPortText));
                    node.addListener(new NodeErrLogger());
                } catch (Exception ex) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Failure");
                    alert.setContentText("Port unavailable");
                    alert.showAndWait();
                    return;
                }

                if (connectToExistingChatCheckBox.isSelected()) {
                    String peerAddressText = peerAddressTextField.getText();
                    String peerPortText = peerPortTextField.getText();

                    try {
                        InetSocketAddress peerAddress = new InetSocketAddress(peerAddressText, Integer.parseInt(peerPortText));
                        node.connect(peerAddress);
                    } catch (Exception ex) {
                        node.shutdown();

                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Failure");
                        alert.setContentText("Failed to connect to " + peerAddressText + ":" + peerPortText);
                        alert.showAndWait();
                        return;
                    }
                }

                bindNode(node);

            } else {
                unbindNode();
            }
        });

        this.nodeProperty.addListener((obs, old, val) -> {
            connectToExistingChatCheckBox.selectedProperty().setValue(!connectToExistingChatCheckBox.isSelected());
            connectToExistingChatCheckBox.selectedProperty().setValue(!connectToExistingChatCheckBox.isSelected());

            selfPortTextField.setDisable(val != null);
            connectToExistingChatCheckBox.setDisable(val != null);
            if (val != null) {
                connectivityButton.setText("Disconnect");
                discoveredPeersListView.setDisable(true);
                peerAddressTextField.setDisable(true);
                peerPortTextField.setDisable(true);
            }

            chatTitledPane.setDisable(val == null);
            chatTitledPane.setExpanded(val != null);

            if (val != null) {
                chatTextFlow.getChildren().clear();
            }
        });

        peerDiscoveryTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                List<InetSocketAddress> discoveredPeers = BrowserClient.getAvailablePeers();

                Platform.runLater(() -> {
                    discoveredPeersListView.getItems().clear();
                    discoveredPeersListView.getItems().addAll(discoveredPeers.stream().sorted(InetSocketAddressUtils::compare)
                            .map(InetSocketAddressUtils::toString)
                            .collect(Collectors.toList()));
                });
            }
        }, 0, 1000);

        discoveredPeersListView.getSelectionModel().selectedIndexProperty().addListener((observableValue, s, t1) -> {
            if (t1.intValue() == -1) {
                return;
            }

            peerAddressTextField.setText(discoveredPeersListView.getItems().get(t1.intValue()).split(":")[0]);
            peerPortTextField.setText(discoveredPeersListView.getItems().get(t1.intValue()).split(":")[1]);
        });
    }

    @Override
    public void onPeerConnected(InetSocketAddress peerAddress) {
        updatePeers();
        addMessageToChatView(SYSTEM_SENDER, String.format("%s connected", InetSocketAddressUtils.toString(peerAddress)), COLOR_GOOD, false, false);
    }

    @Override
    public void onPeerDisconnected(InetSocketAddress peerAddress) {
        updatePeers();
        addMessageToChatView(SYSTEM_SENDER, String.format("%s disconnected", InetSocketAddressUtils.toString(peerAddress)), COLOR_BAD, false, false);
    }

    @Override
    public void onMessageReceived(InetSocketAddress peerAddress, String message) {
        String sender;

        if (InetSocketAddressUtils.equals(peerAddress, nodeProperty.getValue().getSelfAddress())) {
            sender = SELF_SENDER_NAME;
        } else {
            sender = InetSocketAddressUtils.toString(peerAddress);
        }

        addMessageToChatView(sender, message, COLOR_NORMAL, false, false);
    }

    @Override
    public void onMessageSent(String message) {
        addMessageToChatView(SELF_SENDER_NAME, message, "black", false, true);
    }

    @Override
    public void onMessageConsensus() {
        addMessageToChatView(SYSTEM_SENDER, "consensus discovered", COLOR_WARN, true, false);
    }

    @Override
    public void onConnect(InetSocketAddress peerAddress) {

    }

    @Override
    public void onConnectSuccess(InetSocketAddress peerAddress) {

    }

    @Override
    public void onConnectFail(InetSocketAddress peerAddress) {

    }

    @Override
    public void onShutdown() {

    }

    private void bindNode(Node node) {
        Objects.requireNonNull(node);

        new Thread(() -> {
            try {
                browserServer = new BrowserServer(node.getSelfPort());
                browserServer.listen();
            } catch (SocketException ignored) {

            }
        }).start();

        node.addListener(this);
        updatePeers();

        this.nodeProperty.setValue(node);

        syncTimer = new Timer();
        syncTimer.scheduleAtFixedRate(new TimerTask() {
            private boolean participateInConsensus = false;

            @Override
            public void run() {
                try {
                    node.synchronize(participateInConsensus);
                    participateInConsensus = true;
                } catch (Exception ignored) {

                }
            }
        }, 0, 1000);
    }

    private void unbindNode() {
        if (browserServer != null) {
            browserServer.shutdown();
        }

        syncTimer.cancel();
        nodeProperty.getValue().shutdown();

        browserServer = null;
        syncTimer = null;
        nodeProperty.setValue(null);
    }

    public void shutdown() {
        peerDiscoveryTimer.cancel();

        if (browserServer != null) {
            browserServer.shutdown();
        }

        if (syncTimer != null) {
            syncTimer.cancel();
        }

        if (nodeProperty.getValue() != null) {
            nodeProperty.getValue().shutdown();
        }
    }

    private void updatePeers() {
        Platform.runLater(() -> {
            List<String> peers = new ArrayList<>();

            InetSocketAddress selfAddress = nodeProperty.getValue().getSelfAddress();

            peers.add((selfAddress != null ? InetSocketAddressUtils.toString(selfAddress) : "unknown") + " (me)");

            for (InetSocketAddress address : nodeProperty.getValue().getRemoteAddresses()) {
                peers.add(InetSocketAddressUtils.toString(address));
            }

            peerListView.setItems(FXCollections.observableList(peers));
        });
    }

    private void addMessageToChatView(String sender, String message, String color, boolean clearChat, boolean clearInput) {
        Platform.runLater(() -> {
            Text senderText = new Text();
            senderText.setText(String.format("%s: ", sender));
            senderText.setStyle("-fx-font-weight: bold;" + "-fx-fill: " + color);

            Text messageText = new Text();
            messageText.setText(String.format("%s\n", message));
            messageText.setStyle("-fx-fill: " + color);

            if (clearChat) {
                chatTextFlow.getChildren().clear();
            }
            if (clearInput) {
                inputTextField.clear();
            }

            chatTextFlow.getChildren().addAll(senderText, messageText);
            chatScrollPane.setVvalue(1.0);
        });
    }
}