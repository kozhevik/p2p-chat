package com.kozhevik.p2pchat;

/**
 * Main file
 * We start this file
 */

public class AppLauncher {

    public static void main(String[] args) {
        ChatApplication.main(args);
    }
}
