package com.kozhevik.p2pchat.utils;

import com.kozhevik.p2pchat.p2p.NodeListener;

import java.net.InetSocketAddress;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Logger (writes logs to file)
 */

public class NodeErrLogger implements NodeListener {

    @Override
    public void onPeerConnected(InetSocketAddress peerAddress) {
        log(String.format("node '%s' connected", InetSocketAddressUtils.toString(peerAddress)));
    }

    @Override
    public void onPeerDisconnected(InetSocketAddress peerAddress) {
        log(String.format("node '%s' disconnected", InetSocketAddressUtils.toString(peerAddress)));
    }

    @Override
    public void onMessageReceived(InetSocketAddress peerAddress, String message) {
        log(String.format("received message from node '%s': %s", InetSocketAddressUtils.toString(peerAddress), message));
    }

    @Override
    public void onMessageSent(String message) {
        log(String.format("sent message: %s", message));
    }

    @Override
    public void onMessageConsensus() {
        log("received new consensus");
    }

    @Override
    public void onConnect(InetSocketAddress peerAddress) {
        log(String.format("connecting to '%s'", InetSocketAddressUtils.toString(peerAddress)));
    }

    @Override
    public void onConnectSuccess(InetSocketAddress peerAddress) {
        log(String.format("successfully connected to '%s'", InetSocketAddressUtils.toString(peerAddress)));
    }

    @Override
    public void onConnectFail(InetSocketAddress peerAddress) {
        log(String.format("failed to connect to '%s'", InetSocketAddressUtils.toString(peerAddress)));
    }

    @Override
    public void onShutdown() {
        log("disconnected");
    }

    private void log(String msg) {
        System.err.printf("[%s] %s%n", getFormattedDate(), msg);
    }

    private String getFormattedDate() {
        return new SimpleDateFormat("MM-dd-yyyy hh:mm:ss").format(new Date());
    }
}
