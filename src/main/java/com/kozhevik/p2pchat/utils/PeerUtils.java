package com.kozhevik.p2pchat.utils;

import com.kozhevik.p2pchat.p2p.Peer;

import java.net.InetSocketAddress;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;
import java.util.Objects;

/**
 * For broadcast
 */

public abstract class PeerUtils {
    public static <T> T cast(InetSocketAddress remoteAddress, Cast<T> cast) {
        Objects.requireNonNull(remoteAddress);
        Objects.requireNonNull(cast);

        Peer remotePeer;

        Registry remoteRegistry;
        try {
            remoteRegistry = LocateRegistry.getRegistry(remoteAddress.getAddress().getHostAddress(), remoteAddress.getPort());
            remotePeer = (Peer) remoteRegistry.lookup(Peer.BINDING_NAME);

            return cast.execute(remotePeer);
        } catch (RemoteException | NotBoundException e) {
            cast.unavailable();
        }

        return null;
    }

    public static void broadcast(List<InetSocketAddress> remoteAddresses, Broadcast broadcast) {
        Objects.requireNonNull(remoteAddresses);
        Objects.requireNonNull(broadcast);

        for (InetSocketAddress remoteAddress : remoteAddresses) {
            if (remoteAddress == null) {
                continue;
            }

            Peer remotePeer;

            Registry remoteRegistry;
            try {
                remoteRegistry = LocateRegistry.getRegistry(remoteAddress.getAddress().getHostAddress(), remoteAddress.getPort());
                remotePeer = (Peer) remoteRegistry.lookup(Peer.BINDING_NAME);

                broadcast.execute(remotePeer, remoteAddress);
            } catch (RemoteException | NotBoundException e) {
                broadcast.unavailable(remoteAddress);
            }
        }
    }

    public interface Cast<T> {
        T execute(Peer peer) throws RemoteException;

        default void unavailable() {
        }
    }

    public interface Broadcast {
        void execute(Peer peer, InetSocketAddress address) throws RemoteException;

        void unavailable(InetSocketAddress unavailableAddress);
    }
}
