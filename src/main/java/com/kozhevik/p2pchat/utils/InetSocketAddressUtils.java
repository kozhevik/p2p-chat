package com.kozhevik.p2pchat.utils;

import java.net.InetSocketAddress;
import java.util.StringJoiner;

/**
 * For addresses: compare and transform to string
 */

public abstract class InetSocketAddressUtils {
    private static Integer extractAddress(InetSocketAddress addr) {
        byte[] a = addr.getAddress().getAddress();
        return ((a[0] & 0xff) << 24) | ((a[1] & 0xff) << 16) | ((a[2] & 0xff) << 8) | (a[3] & 0xff);
    }

    public static int compare(InetSocketAddress o1, InetSocketAddress o2) {
        if (o1 == o2) {
            return 0;
        } else if (o1.isUnresolved() || o2.isUnresolved()) {
            return o1.toString().compareTo(o2.toString());
        } else {
            int compare = extractAddress(o1).compareTo(extractAddress(o2));
            if (compare == 0) {
                compare = Integer.compare(o1.getPort(), o2.getPort());
            }
            return compare;
        }
    }

    public static boolean equals(InetSocketAddress o1, InetSocketAddress o2) {
        return compare(o1, o2) == 0;
    }

    public static String toString(InetSocketAddress o1) {
        StringJoiner stringJoiner = new StringJoiner(":");

        stringJoiner.add(o1.getAddress().getHostAddress());
        stringJoiner.add(Integer.toString(o1.getPort()));

        return stringJoiner.toString();
    }
}
