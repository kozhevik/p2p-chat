package com.kozhevik.p2pchat;

import com.kozhevik.p2pchat.controller.MainController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * File for GUI - JavaFX
 */

public class ChatApplication extends Application {
    private MainController mainController;

    @Override
    public void start(Stage stage) throws IOException {
        System.setErr(new PrintStream(String.format("logs-%s.txt", new SimpleDateFormat("yyyy-MM-dd hh_mm_ss").format(new Date()))));

        FXMLLoader fxmlLoader = new FXMLLoader(ChatApplication.class.getResource("main.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 640, 480);

        mainController = fxmlLoader.getController();
        mainController.init();

        stage.setTitle("p2p chat");
        stage.setScene(scene);
        stage.show();
    }

    @Override
    public void stop() {
        mainController.shutdown();
    }

    public static void main(String[] args) {
        launch(args);
    }
}