package com.kozhevik.p2pchat.p2p;

import com.kozhevik.p2pchat.p2p.model.Discovery;
import com.kozhevik.p2pchat.p2p.model.Message;
import com.kozhevik.p2pchat.utils.InetSocketAddressUtils;
import com.kozhevik.p2pchat.utils.PeerUtils;

import java.net.InetSocketAddress;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.RemoteServer;
import java.rmi.server.ServerNotActiveException;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;

/**
 * Node (a peer)
 * Node is an every client, single client
 */

public class Node implements Peer {
    private final int selfPort;
    private InetSocketAddress selfAddress;
    private final Registry selfRegistry;
    private final List<InetSocketAddress> remoteAddresses;
    private final List<InetSocketAddress> allAddresses;
    private final List<NodeListener> listeners;
    private final List<Message> messages;
    private boolean disposed;

    private final List<InetSocketAddress> trustedAddresses = new ArrayList<>();

    public Node(int selfPort) {
        this.selfPort = selfPort;
        this.selfAddress = null;
        this.messages = new ArrayList<>();
        this.listeners = new ArrayList<>();

        try {
            this.selfRegistry = LocateRegistry.createRegistry(selfPort);

            UnicastRemoteObject.exportObject(this, selfPort);
            this.selfRegistry.bind(Peer.BINDING_NAME, this);
        } catch (RemoteException | AlreadyBoundException e) {
            throw new RuntimeException("Unable to bind registry", e);
        }

        this.remoteAddresses = new ArrayList<>();
        this.allAddresses = new ArrayList<>();
        this.disposed = false;
    }

    @Override
    public synchronized Discovery discover(int port) {
        disposeGuard();

        InetSocketAddress clientAddress;

        try {
            clientAddress = new InetSocketAddress(RemoteServer.getClientHost(), port);
        } catch (ServerNotActiveException e) {
            throw new RuntimeException(e);
        }

        InetSocketAddress retrievedAddress = PeerUtils.cast(clientAddress, peer -> peer.retrieveAddress(selfPort));

        if (selfAddress == null) {
            selfAddress = retrievedAddress;
            allAddresses.add(retrievedAddress);

            for (Message msg : messages) {
                if (msg.getSender() == null) {
                    msg.setSender(selfAddress);
                }
            }
        }

        if (!InetSocketAddressUtils.equals(selfAddress, retrievedAddress)) {
            return null;
        }

        if (remoteAddresses.stream().anyMatch(x -> InetSocketAddressUtils.equals(x, clientAddress))) {
            remoteAddresses.removeIf(x -> InetSocketAddressUtils.equals(x, clientAddress));
            allAddresses.removeIf(x -> InetSocketAddressUtils.equals(x, clientAddress));

            for (NodeListener listener : listeners) {
                listener.onPeerDisconnected(clientAddress);
            }
        }

        Discovery discovery = new Discovery(clientAddress, selfAddress, new ArrayList<>(remoteAddresses));

        remoteAddresses.add(clientAddress);
        allAddresses.add(clientAddress);

        for (NodeListener listener : listeners) {
            listener.onPeerConnected(clientAddress);
        }

        return discovery;
    }

    @Override
    public InetSocketAddress retrieveAddress(int port) throws RemoteException {
        InetSocketAddress clientAddress;

        try {
            clientAddress = new InetSocketAddress(RemoteServer.getClientHost(), port);
        } catch (ServerNotActiveException e) {
            throw new RuntimeException(e);
        }

        return clientAddress;
    }

    @Override
    public int messagesHashCode() throws RemoteException {
        disposeGuard();

        return messages.hashCode();
    }

    @Override
    public List<Message> messages() throws RemoteException {
        disposeGuard();

        return messages;
    }

    // Node calls this method on every other Node
    @Override
    public synchronized void message(String message, int port) throws RemoteException {
        disposeGuard();

        InetSocketAddress address;

        try {
            address = new InetSocketAddress(RemoteServer.getClientHost(), port);
        } catch (ServerNotActiveException e) {
            throw new RuntimeException(e);
        }

        if (remoteAddresses.stream().noneMatch(x -> InetSocketAddressUtils.equals(x, address))) {
            return;
        }

//        if (message.isBlank()) {
//            return;
//        }

        messages.add(new Message(address, message));

        for (NodeListener listener : listeners) {
            listener.onMessageReceived(address, message);
        }
    }

    @Override
    public void leave(int port) throws RuntimeException {
        disposeGuard();

        InetSocketAddress address;

        try {
            address = new InetSocketAddress(RemoteServer.getClientHost(), port);
        } catch (ServerNotActiveException e) {
            throw new RuntimeException(e);
        }

        remoteAddresses.removeIf(x -> InetSocketAddressUtils.equals(x, address));
        allAddresses.removeIf(x -> InetSocketAddressUtils.equals(x, address));

        for (NodeListener listener : listeners) {
            listener.onPeerDisconnected(address);
        }
    }

    public synchronized void connect(InetSocketAddress originAddress) {
        Objects.requireNonNull(originAddress);

        disposeGuard();

        if (!remoteAddresses.isEmpty()) {
            throw new RuntimeException("already connected");
        }
        for (NodeListener listener : listeners) {
            listener.onConnect(originAddress);
        }

        Discovery discovery;

        try {
            Registry originRegistry = LocateRegistry.getRegistry(originAddress.getAddress().getHostAddress(), originAddress.getPort());
            Peer originPeer = (Peer) originRegistry.lookup(Peer.BINDING_NAME);
            discovery = originPeer.discover(selfPort);

            if (discovery == null) {
                for (NodeListener listener : listeners) {
                    listener.onConnectFail(originAddress);
                }
                throw new RuntimeException("Different networks");
            }
        } catch (RemoteException | NotBoundException e) {
            for (NodeListener listener : listeners) {
                listener.onConnectFail(originAddress);
            }
            throw new RuntimeException("Unable to establish connection", e);
        }

        ArrayList<InetSocketAddress> remoteAddresses = new ArrayList<>(discovery.getRemoteAddresses());

        PeerUtils.broadcast(new ArrayList<>(remoteAddresses), new PeerUtils.Broadcast() {
            @Override
            public void execute(Peer peer, InetSocketAddress address) throws RemoteException {
                peer.discover(selfPort);
            }

            @Override
            public void unavailable(InetSocketAddress unavailableAddress) {
                remoteAddresses.remove(unavailableAddress);
                allAddresses.remove(unavailableAddress);
            }
        });

        this.selfAddress = discovery.getClientAddress();
        this.remoteAddresses.add(discovery.getRemoteAddress());
        this.remoteAddresses.addAll(remoteAddresses);

        this.allAddresses.add(discovery.getRemoteAddress());
        this.allAddresses.addAll(remoteAddresses);
        this.allAddresses.add(discovery.getClientAddress());

        for (NodeListener listener : listeners) {
            listener.onConnectSuccess(originAddress);
        }
    }

    public synchronized void shutdown() {
        disposeGuard();

        PeerUtils.broadcast(new ArrayList<>(remoteAddresses), new PeerUtils.Broadcast() {
            @Override
            public void execute(Peer peer, InetSocketAddress address) throws RemoteException {
                peer.leave(selfPort);
            }

            @Override
            public void unavailable(InetSocketAddress unavailableAddress) {
            }
        });

        try {
            UnicastRemoteObject.unexportObject(this, true);
            UnicastRemoteObject.unexportObject(selfRegistry, true);
            selfRegistry.unbind(Peer.BINDING_NAME);
        } catch (RemoteException | NotBoundException e) {
            throw new RuntimeException("Unable to disconnect", e);
        }

        disposed = true;
        for (NodeListener listener : listeners) {
            listener.onShutdown();
        }
    }

    public synchronized void synchronize(boolean participateInConsensus) {
        disposeGuard();

        HashMap<Integer, List<InetSocketAddress>> consensusToPeer = new HashMap<>();

        List<InetSocketAddress> consensusAddresses = new ArrayList<>(allAddresses);

        if (consensusAddresses.size() % 2 == 0) {
            consensusAddresses.remove(allAddresses.size() - 1);

        }
        consensusAddresses.remove(selfAddress);

        if (participateInConsensus) {
            consensusToPeer.put(messages.hashCode(), new ArrayList<>(Collections.singletonList(null)));
        }

        PeerUtils.broadcast(new ArrayList<>(consensusAddresses), new PeerUtils.Broadcast() {
            @Override
            public void execute(Peer peer, InetSocketAddress address) throws RemoteException {
                Integer consensus = peer.messagesHashCode();
                List<InetSocketAddress> count = consensusToPeer.get(consensus);

                if (count == null) {
                    consensusToPeer.put(consensus, new ArrayList<>(Collections.singletonList(address)));
                } else {
                    count.add(address);
                }
            }

            @Override
            public void unavailable(InetSocketAddress unavailableAddress) {
                remoteAddresses.remove(unavailableAddress);
                consensusAddresses.remove(unavailableAddress);
                allAddresses.remove(unavailableAddress);

                for (NodeListener listener : listeners) {
                    listener.onPeerDisconnected(unavailableAddress);
                }
            }
        });

        Map.Entry<Integer, List<InetSocketAddress>> best = null;
        for (Map.Entry<Integer, List<InetSocketAddress>> pair : consensusToPeer.entrySet()) {
            if (best == null || pair.getValue().size() > best.getValue().size()) {
                best = pair;
            }
        }


        if (best == null || best.getKey() == Arrays.hashCode(messages.toArray())) {
            return;
        }

        acquireConsensusMessages(best.getKey(), best.getValue());
    }

    public synchronized void send(String message) {
        disposeGuard();

        messages.add(new Message(selfAddress, message));

        PeerUtils.broadcast(new ArrayList<>(remoteAddresses), new PeerUtils.Broadcast() {
            @Override
            public void execute(Peer peer, InetSocketAddress address) throws RemoteException {
                peer.message(message, selfPort);
            }

            @Override
            public void unavailable(InetSocketAddress unavailableAddress) {
                remoteAddresses.remove(unavailableAddress);
                allAddresses.remove(unavailableAddress);

                for (NodeListener listener : listeners) {
                    listener.onPeerDisconnected(unavailableAddress);
                }
            }
        });

        for (NodeListener listener : listeners) {
            listener.onMessageSent(message);
        }
    }

    public synchronized InetSocketAddress getSelfAddress() {
        disposeGuard();

        return selfAddress;
    }

    public int getSelfPort() {
        return selfPort;
    }

    public synchronized List<InetSocketAddress> getRemoteAddresses() {
        disposeGuard();

        return new ArrayList<>(remoteAddresses);
    }

    public synchronized void addListener(NodeListener listener) {
        disposeGuard();

        listeners.add(listener);
    }

    private void acquireConsensusMessages(Integer hashCode, List<InetSocketAddress> peers) {
        PeerUtils.broadcast(peers, new PeerUtils.Broadcast() {
            private boolean acquired = false;

            @Override
            public void execute(Peer peer, InetSocketAddress address) throws RemoteException {
                if (acquired) {
                    return;
                }

                List<Message> receivedMessages = peer.messages();

                int receivedMessagesHashCode = Arrays.hashCode(receivedMessages.toArray());

                if (hashCode != null && hashCode != receivedMessagesHashCode) {
                    return;
                }

                messages.clear();

                for (NodeListener listener : listeners) {
                    listener.onMessageConsensus();
                }

                for (Message receivedMessage : receivedMessages) {
                    messages.add(receivedMessage);

                    for (NodeListener listener : listeners) {
                        listener.onMessageReceived(receivedMessage.getSender(), receivedMessage.getMessage());
                    }
                }

                acquired = true;
            }

            @Override
            public void unavailable(InetSocketAddress unavailableAddress) {
                remoteAddresses.remove(unavailableAddress);
                allAddresses.remove(unavailableAddress);

                for (NodeListener listener : listeners) {
                    listener.onPeerDisconnected(unavailableAddress);
                }
            }
        });
    }

    private void disposeGuard() {
        if (disposed) {
            throw new RuntimeException("Node can not be used (disposed)");
        }
    }
}
