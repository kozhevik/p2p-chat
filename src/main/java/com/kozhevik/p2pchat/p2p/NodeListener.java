package com.kozhevik.p2pchat.p2p;

import java.net.InetSocketAddress;

/**
 * Interface for working with message-events (logs or
 */

public interface NodeListener {
    void onPeerConnected(InetSocketAddress peerAddress);

    void onPeerDisconnected(InetSocketAddress peerAddress);

    void onMessageReceived(InetSocketAddress peerAddress, String message);

    void onMessageSent(String message);

    void onMessageConsensus();

    void onConnect(InetSocketAddress peerAddress);

    void onConnectSuccess(InetSocketAddress peerAddress);

    void onConnectFail(InetSocketAddress peerAddress);

    void onShutdown();
}
