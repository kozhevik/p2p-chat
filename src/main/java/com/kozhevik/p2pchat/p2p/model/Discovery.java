package com.kozhevik.p2pchat.p2p.model;

import java.io.Serializable;
import java.net.InetSocketAddress;
import java.util.Collections;
import java.util.List;

public class Discovery implements Serializable {
    private InetSocketAddress clientAddress;
    private InetSocketAddress remoteAddress;
    private List<InetSocketAddress> remoteAddresses;

    public Discovery(InetSocketAddress clientAddress, InetSocketAddress remoteAddress, List<InetSocketAddress> remoteAddresses) {
        this.clientAddress = clientAddress;
        this.remoteAddress = remoteAddress;
        this.remoteAddresses = remoteAddresses;
    }

    public InetSocketAddress getClientAddress() {
        return clientAddress;
    }

    public void setClientAddress(InetSocketAddress clientAddress) {
        this.clientAddress = clientAddress;
    }

    public List<InetSocketAddress> getRemoteAddresses() {
        return Collections.unmodifiableList(remoteAddresses);
    }

    public void setRemoteAddresses(List<InetSocketAddress> remoteAddresses) {
        this.remoteAddresses = Collections.unmodifiableList(remoteAddresses);
    }

    public InetSocketAddress getRemoteAddress() {
        return remoteAddress;
    }

    public void setRemoteAddress(InetSocketAddress remoteAddress) {
        this.remoteAddress = remoteAddress;
    }
}
