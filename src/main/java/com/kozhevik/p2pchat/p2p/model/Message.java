package com.kozhevik.p2pchat.p2p.model;

import java.io.Serializable;
import java.net.InetSocketAddress;

/**
 * Message
 */

public class Message implements Serializable {
    private InetSocketAddress sender;
    private String message;

    public Message(InetSocketAddress sender, String message) {
        this.sender = sender;
        this.message = message;
    }

    public InetSocketAddress getSender() {
        return sender;
    }

    public void setSender(InetSocketAddress sender) {
        this.sender = sender;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Message message1 = (Message) o;

        if (!sender.equals(message1.sender)) return false;
        return message.equals(message1.message);
    }

    @Override
    public int hashCode() {
        int result = sender.hashCode();
        result = 31 * result + message.hashCode();
        return result;
    }
}
