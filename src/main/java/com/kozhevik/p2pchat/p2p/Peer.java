package com.kozhevik.p2pchat.p2p;

import com.kozhevik.p2pchat.p2p.model.Discovery;
import com.kozhevik.p2pchat.p2p.model.Message;

import java.net.InetSocketAddress;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 * Interface for Peer
 */

public interface Peer extends Remote {
    String BINDING_NAME = "p2pchat.peer";

    Discovery discover(int port) throws RemoteException;

    InetSocketAddress retrieveAddress(int port) throws RemoteException;

    int messagesHashCode() throws RemoteException;

    List<Message> messages() throws RemoteException;

    void message(String message, int port) throws RemoteException;

    void leave(int port) throws RemoteException;
}
