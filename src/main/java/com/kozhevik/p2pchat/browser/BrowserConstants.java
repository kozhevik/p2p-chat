package com.kozhevik.p2pchat.browser;

import java.util.UUID;

/**
 * UUID - for unique instance (for Node)
 */

public abstract class BrowserConstants {
    public static final int BROADCAST_PORT = 3569;
    public static final int PACKET_SIZE = 256;
    public static final int MAGIC = 0xDEAD;
    public static final UUID UNIQUE_ID = UUID.randomUUID();

}
