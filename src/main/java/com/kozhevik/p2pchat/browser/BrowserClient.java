package com.kozhevik.p2pchat.browser;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Client functional for Node
 */

public abstract class BrowserClient {
    public static List<InetSocketAddress> getAvailablePeers() {
        ArrayList<InetSocketAddress> peers = new ArrayList<>();

        try (DatagramSocket socket = new DatagramSocket()) {
            socket.setBroadcast(true);
            byte[] buffer = new byte[BrowserConstants.PACKET_SIZE];
            DatagramPacket packet = new DatagramPacket(buffer, BrowserConstants.PACKET_SIZE, InetAddress.getByName("255.255.255.255"), BrowserConstants.BROADCAST_PORT);

            socket.setSoTimeout(500);
            socket.send(packet);

            while (true) {
                byte[] receiveBuffer = new byte[BrowserConstants.PACKET_SIZE];
                DatagramPacket receivePacket = new DatagramPacket(receiveBuffer, 0, receiveBuffer.length);

                socket.setSoTimeout(500);
                try {
                    socket.receive(receivePacket);
                } catch (IOException ex) {
                    break;
                }

                ObjectInputStream stream = new ObjectInputStream(new ByteArrayInputStream(receiveBuffer, 0, receiveBuffer.length));

                if (stream.readInt() != BrowserConstants.MAGIC) {
                    continue;
                }
                try {
                    UUID id = ((UUID) stream.readObject());
                    if (id.equals(BrowserConstants.UNIQUE_ID)) {
                        continue;
                    }
                } catch (ClassNotFoundException | ClassCastException ignored) {

                }

                int port;
                try {
                    port = stream.readInt();
                } catch (IOException ex) {
                    continue;
                }

                peers.add(new InetSocketAddress(receivePacket.getAddress(), port));
            }

        } catch (IOException ignored) {
        }

        return peers;
    }
}
