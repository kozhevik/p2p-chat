package com.kozhevik.p2pchat.browser;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

/**
 * Server functional for Node
 */

public class BrowserServer {
    private final DatagramSocket socket;
    private final int selfPort;

    public BrowserServer(int selfPort) throws SocketException {
        this.socket = new DatagramSocket(BrowserConstants.BROADCAST_PORT);
        this.selfPort = selfPort;
    }

    public void listen() {
        while (!socket.isClosed()) {
            byte[] buffer = new byte[BrowserConstants.PACKET_SIZE];
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
            try {
                this.socket.setSoTimeout(500);
                socket.receive(packet);

                ByteArrayOutputStream byteResponseStream = new ByteArrayOutputStream();
                ObjectOutputStream responseStream = new ObjectOutputStream(byteResponseStream);

                responseStream.writeInt(BrowserConstants.MAGIC);
                responseStream.writeObject(BrowserConstants.UNIQUE_ID);
                responseStream.writeInt(selfPort);

                responseStream.flush();

                DatagramPacket responsePacket = new DatagramPacket(byteResponseStream.toByteArray(), 0, byteResponseStream.size(), packet.getSocketAddress());

                socket.setSoTimeout(500);
                socket.send(responsePacket);
            } catch (IOException ignored) {
            }
        }
    }

    public void shutdown() {
        socket.close();
    }
}
