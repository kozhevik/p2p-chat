module com.kozhevik.p2pchat {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.rmi;


    opens com.kozhevik.p2pchat to javafx.fxml;
    exports com.kozhevik.p2pchat;
    exports com.kozhevik.p2pchat.controller;
    exports com.kozhevik.p2pchat.p2p.model;
    opens com.kozhevik.p2pchat.controller to javafx.fxml;
    exports com.kozhevik.p2pchat.p2p;
    opens com.kozhevik.p2pchat.p2p to javafx.fxml;
}