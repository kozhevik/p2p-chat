# p2pchat

p2p chat with peer discovery through udp broadcast and simple consensus mechanism

## Consensus
To see how consensus mechanism works run 2 instances (standart app) and 1 instance (disallow-blank-messages branch)
- Instance A (which disallows blank messages) receives blank message and rejects it
- Then A tries to sync it receives from other instances that they accepted blank message
- A forced to accept the message

Otherwise if there are more instances which disallows blank messages it is going to be rejected
